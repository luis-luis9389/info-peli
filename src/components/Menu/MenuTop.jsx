import React from 'react';
import { Link } from 'react-router-dom';
import Logo from '../../assets/svg/logo.svg';

import './menutop.scss';

const MenuTop = () => {
    return (
        <section>
            <nav className="menu-top">
                <ul className="menu-top__ul">
                    <Link className="menu-top__link" to="/" > <img src={Logo} alt="logo" /> </Link>
                    <li className="menu-top__li">
                        <Link className="menu-top__link" to="/" > Inicio </Link>
                    </li>
                    <li className="menu-top__li">
                        <Link className="menu-top__link" to="/new-movies" > Nuevas Peliculas </Link>
                    </li>
                    <li className="menu-top__li">
                        <Link className="menu-top__link" to="/populars" > Populares </Link>
                    </li>
                    <li className="menu-top__li">
                        <Link className="menu-top__link" to="/search" > Buscar </Link>
                    </li>
                </ul>
            </nav>
        </section>
    )
}

export default MenuTop
