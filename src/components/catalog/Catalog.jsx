import React from 'react';
import { Card } from 'antd';
import { Link } from 'react-router-dom';
import Loading from '../loader/Loading';

import './catalog.scss';

const { Meta } = Card;

const Catalog = (props) => {
  const { movies: {results} } = props;
  // if (movies.loading || !movies.result) {
  //   return <Loading />
  // }
  return results.map(movie => (
    <div key={movie.id} className="catalog-movie">
      <Movie movie={movie} />
    </div>
  ))
};


const Movie = ({ movie: { id, title, poster_path } }) => {
  const posterPath = `https://image.tmdb.org/t/p/original${poster_path}`;

  return (
    <Link to={`/movie/${id}`}>
      <Card
        style={{ width: 150 }}
        hoverable
        cover={<img src={posterPath} alt={title} />}
      >
        <Meta title={title} />
      </Card>
    </Link>
  )
};

export default Catalog
