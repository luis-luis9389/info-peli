import React from 'react';
import { Carousel, Button } from 'antd';
import { Link } from 'react-router-dom';
import Loading from '../loader/Loading';

import './carousel.scss';

const CarouselHome = ({ movies }) => {

  if (movies.loading || !movies.result) {
    return <Loading />
  }

  const { results } = movies.result;

  return (
    <Carousel autoplay effect="fade" className="carousel-movies" >
      { results.map(movie => (
        <Movie movie={movie} key={movie.id} />
      ))}
    </Carousel>
  )
}

const Movie = ({ movie: { id, backdrop_path, title, overview } }) => {
  const poster = `https://image.tmdb.org/t/p/original${backdrop_path}`;

  return (
    <div className="carousel-movies__movie" style={{ backgroundImage: `url('${poster}')` }} >
      <div className="carousel-movies__info">
        <div>
          <h2> {title} </h2>
          <p> {overview} </p>
          <div>
            <Link to={`movie/${id}`} >
              <Button type="primary" >
                Ver más
            </Button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CarouselHome
