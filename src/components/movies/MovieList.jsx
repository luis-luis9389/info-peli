import React from 'react';
import Loading from '../loader/Loading';
import Catalog from '../catalog/Catalog';

import './movielist.scss';

const MovieList = ({ title, movies }) => {

  if (movies.loading || !movies.result) {
    return <Loading />
  }

  return (
    <section className="movie-list">
      <Catalog movies={movies.result} />
    </section>
  )
};

export default MovieList;

