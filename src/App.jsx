import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import MenuTop from './components/Menu/MenuTop';
import Footer from './components/Footer/Footer';

// pages
import Home from './pages/home/home';
import NuevasPeliculas from './pages/nuevas';
import Populares from './pages/populares';
import Pelicula from './pages/pelicula/pelicula';
import Buscador from './pages/buscador';
import Error404 from './pages/error404';

function App() {
  return (
    <>
      <Router>
        <MenuTop />
        <Switch>
          <Route path="/" exact={true} >
            <Home />
          </Route>
          <Route path="/new-movies" exact={true} >
            <NuevasPeliculas />
          </Route>
          <Route path="/populars" exact={true} >
            <Populares />
          </Route>
          <Route path="/search" exact={true} >
            <Buscador />
          </Route>
          <Route path="/movie/:id" exact={true} >
            <Pelicula />
          </Route>
          <Route path="*" exact={true} >
            <Error404 />
          </Route>
        </Switch>
        <Footer />
      </Router>
    </>
  );
}

export default App;
