import React from 'react';
import { API_URL, KEY } from '../utils/constantes';
import useFetch from '../hooks/useFetch';
import MovieList from '../components/movies/MovieList';

const Populares = () => {

  const popularMovies = useFetch(`${API_URL}/movie/popular?api_key=${KEY}&language=es-ES&page=1`);


  return (
    <section>
      <MovieList movies={popularMovies} />
    </section>
  )
}

export default Populares
