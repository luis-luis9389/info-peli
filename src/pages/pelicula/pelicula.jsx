import React from 'react';
import { API_URL, KEY } from '../../utils/constantes';
import useFetch from '../../hooks/useFetch';
import { useParams } from 'react-router-dom';
import Loading from '../../components/loader/Loading';
import moment from 'moment';
import { Progress } from 'antd';

import './pelicula.scss';

const Pelicula = () => {
  const { id } = useParams();
  const movieInfo = useFetch(`${API_URL}/movie/${id}?api_key=${KEY}&language=es-ES`);
  console.log(movieInfo);
  if (movieInfo.loading || !movieInfo.result) {
    return <Loading />
  }
  return (
    <RenderMovie movieInfo={movieInfo.result} />
  )
}

const RenderMovie = (props) => {
  const { movieInfo: { backdrop_path, poster_path } } = props;
  const backdrop_Path = `https://image.tmdb.org/t/p/original${backdrop_path}`;

  return (
    <div className="movie" style={{ backgroundImage: `url('${backdrop_Path}')` }}>
      <div className="movie__dark" />
      <section className="movie__grid">
        <div className="movie__poster">
          <PosterMovie image={poster_path} />
        </div>
        <div className="movie__info">
          <MovieInfo movieInfo={props.movieInfo} />
        </div>
      </section>
    </div>
  )
};

const PosterMovie = ({ image }) => {

  const posterPath = `https://image.tmdb.org/t/p/original${image}`;
  return (
    <div style={{ backgroundImage: `url('${posterPath}')` }} />
  )
};

const MovieInfo = ({ movieInfo: { id, title, release_date, overview, genres, vote_average } }) => {
  return (
    <>
      <div className="movie__info-header">
        <h1>
          {title}
          <span> ({moment(release_date, "YYYY-MM-DD").format("YYYY")}) </span>
        </h1>
        <div className="movie__genre">
          <h3> Generos: </h3>
          <ul>
            {genres.map(gender => (
              <li key={gender.id} > {gender.name} </li>
            ))}
          </ul>
        </div>
        <div>
          <Progress
            type="circle"
            strokeColor={{
              '0%': '#108ee9',
              '100%': '#87d068',
            }}
            percent={parseInt(Math.round(vote_average / 100 * 1000))}
          />
        </div>
        {/* {renderVideo()} */}
      </div>
      <div className="movie__info-content">
        <h3>Resumen</h3>
        <p> {overview} </p>
      </div>
    </>
  )
}

export default Pelicula;

