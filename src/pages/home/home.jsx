import React from 'react';
import CarouselHome from '../../components/slider/CarouselHome';
import useFetch from '../../hooks/useFetch';
import { API_URL, KEY } from '../../utils/constantes';
import MovieList from '../../components/movies/MovieList';

import './home.scss';

const Home = () => {

  const popularMovies = useFetch(`${API_URL}/movie/popular?api_key=${KEY}&language=es-ES&page=1`);
  const topMovies = useFetch(`${API_URL}/movie/top_rated?api_key=${KEY}&language=es-Es&page=1`);

  return (
    <>
      <CarouselHome movies={popularMovies} />
      <h1>Top Mejores Películas</h1>
      <section>
        <MovieList title='Top Mejores Películas' movies={topMovies} />
      </section>
    </>
  )
}

export default Home;
