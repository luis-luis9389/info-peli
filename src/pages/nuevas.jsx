import React from 'react';
import Catalog from '../components/catalog/Catalog';
import MovieList from '../components/movies/MovieList';
import useFetch from '../hooks/useFetch';
import { API_URL, KEY } from '../utils/constantes';

const Nuevas = () => {

  const newMovies = useFetch(`${API_URL}/movie/now_playing?api_key=${KEY}&language=es-Es&page=1`);

  return (
    <section>
      <MovieList movies={newMovies}/>
    </section>
  )
}

export default Nuevas;

